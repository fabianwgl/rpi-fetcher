const express = require('express');
const app = express();

const config = require('../config');
const pkg = require('../../package.json');

app.get('/', function (req, res) {
  res.json({version: pkg.version});
});

app.listen(config.port, function () {
  console.log(`App listening on port ${config.port}`);
});
