'use strict';

const path = require('path');
const dotenv = require('dotenv');
const mongoose = require('mongoose');

const pkg = require('../package.json');

dotenv.config({silent: true, path: path.join(__dirname, '..', '.env')});

// Conexion de mongodb
const db = process.env.MONGODB_URI || 'mongodb://localhost/rpi';
mongoose.connect(db);
mongoose.Promise = global.Promise;

module.exports = {
  env: process.env.NODE_ENV || 'development',
  port: process.env.PORT || 3000,
  version: pkg.version
};