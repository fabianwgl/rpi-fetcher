'use strict';

const Twitter = require('twitter');
const moment = require('moment');
const request = require('request');

const LogModel = require('../db/model');
const url = process.env.API_URL

var client = new Twitter({
  consumer_key: process.env.TWITTER_CONSUMER_KEY,
  consumer_secret: process.env.TWITTER_CONSUMER_SECRET,
  access_token_key: process.env.TWITTER_ACCESS_TOKEN_KEY,
  access_token_secret: process.env.TWITTER_ACCESS_TOKEN_SECRET,
});

const sendToApi = (tweet,cb) => {
  request.post(
    url,
    {json:tweet},
    (err,res,body)=>{
    console.log(body);
  });
}

/**
 * Envía los posibles retweets
 **/

  client.stream('statuses/filter', {language:'es',track: 'paine,aculeo,pintue,lomas del aguila,Águila sur'}, (stream) => {
  console.log('fetcher init');
  stream.on('data', (tweet) => {

      //Si el tweet contiene alguna de las palabras de abajo, no mostrar
    if(tweet.text.indexOf("torres") > -1
      || tweet.text.indexOf("Torres") > -1
      || tweet.user.screen_name=="huiti_time"
      || tweet.text.indexOf("Tricolor") > -1
      || tweet.text.indexOf("tricolor") > -1
      || tweet.text.indexOf("RT") > -1
      || tweet.text.indexOf("Cuernos") > -1
      || tweet.text.indexOf("cuernos") > -1
      || tweet.text.indexOf("Thomas Paine") > -1
      || tweet.text.indexOf("thomas paine") > -1
      || tweet.text.indexOf("Paineñanco") > -1
      || tweet.text.indexOf("paineñanco") > -1
      ){
      //No se muestra mensaje
      console.log('not');
    }else{
      let log = {
        source: 'rpi-nataniel',
        created_at: moment().format()
      }

      sendToApi(tweet, (err, response, body) =>{
        if(err){
          log['message'] = err;
        }else{
          log['message'] = response;
        }
      });
      LogModel.create(log,(err, response) =>{
        if(err) console.log(err);
        console.log(response);
      })
    }
  });
  stream.on('error', function(error) {
    console.log("Error twitter: "+error);
  });
});


