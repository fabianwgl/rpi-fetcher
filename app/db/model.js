'use strict';

const mongoose = require('mongoose');

const LogSchema = new mongoose.Schema({
  source: String,
  message: String,
  created_at: String
});

LogSchema.statics.get = (id, cb) => {
  const query = {_id: id};
  this.findOne(query,cb);
}

LogSchema.statics.create = function( data, cb) {
  const Log = new this({
    source: data.source,
    message: data.message,
    created_at: data.created_at
  });
  Log.save(cb);
};

module.exports = mongoose.model('Log', LogSchema);
